#include <stdio.h>

int header(FILE *f, int *w, int *h, int *d) {
  if (2 == fscanf(f, "P6\n%d %d\n255", w, h)) {
    if ('\n' == getc(f)) {
      *d = 255;
      return 1;
    }
  }
  return 0;
}

int main(int argc, char **argv) {
  int w = 0, h = 0, d = 0;
  while (header(stdin, &w, &h, &d)) {
    if (d != 255) {
      break;
    }
    fprintf(stdout, "P6\n%d %d\n%d\n", w, h, 255);
    for (int j = 0; j < h; ++j) {
      for (int i = 0; i < w; ++i) {
        int r = getc(stdin);
        int g = getc(stdin);
        int b = getc(stdin);
        b = r >> 1;
        putc(r, stdout);
        putc(g, stdout);
        putc(b, stdout);
      }
    }
  }
  return 0;
}
