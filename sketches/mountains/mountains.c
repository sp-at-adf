#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define width 1024
#define height 1024

float mountain[width];

unsigned char pgm[height][width];

static inline int min(int i, int j) { return i < j ? i : j; }
static inline int max(int i, int j) { return i > j ? i : j; }
static inline int clamp(int i, int lo, int hi) { return min(max(i, lo), hi); }

void subdivide(int i0, int i1, float h0, float h1, float dh) {
  if (i0 >= i1 - 1) {
    mountain[i0] = h0;
  } else {
    int i2 = (i0 + i1) / 2;
    float h2 = (h0 + h1) / 2 + dh * (rand() / (double) RAND_MAX - 0.5) * 2;
    subdivide(i0, i2, h0, h2, dh *0.7);
    subdivide(i2, i1, h2, h1, dh *0.7);
  }
}

int main(int argc, char **argv) {
  srand(time(0));
  for (int k = 0; k < 10; ++k) {
    subdivide(0, width, height/2, height/2, height/4);
    memset(&pgm[0][0], 0, width * height);
    #pragma omp parallel for
    for (int j = 0; j < height; ++j) {
      for (int i = 0; i < width; ++i) {
        int n = 0;
        for (int jj = j - 1; jj <= j + 1; ++jj) {
          for (int ii = i - 1; ii <= i + 1; ++ii) {
            n += clamp(jj, 0, height - 1) < mountain[(ii + width) % width];
          }
        }
        pgm[j][i] = (0 < n && n < 9) * 255 + (n < 9) * 0;
      }
    }
    printf("P5\n%d %d\n255\n", width, height);
    fwrite(&pgm[0][0], width * height, 1, stdout);
  }
  return 0;
}
