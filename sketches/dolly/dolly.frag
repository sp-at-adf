#define providesColor

#include "DE-Raytracer.frag"

vec3 baseColor(vec3 p, vec3 n) {
 if (length(p - Target) < 0.11) {  return vec3(1.0, 0.7, 0.0); } else { return vec3(0.0, 1.0, 0.7); }
}

float DE(vec3 p) {
  return min(p.y + 0.01 * (cos(3.0 * p.x) * cos(4.0 * p.z) + cos(5.0 * (p.z - p.x)) * cos(2.0 * (p.z + p.x))) * max(0.1, p.z * p.z / 20.0), length(p - Target) - 0.1);
}
