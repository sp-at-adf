#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define PI 3.141592653589793
#define TILES 8
#define SUBDIVIDE 4
#define SIZE 1024
#define DIAMETER 256
#define DEPTH 0.25
#define DENSITY 0.666
#define COUNT ((DENSITY * SUBDIVIDE * SIZE * SIZE) / (DIAMETER * DIAMETER))
#define RIM (1.5 * PI * 8.0)
#define BOWL (1.0 - 1.5 * PI / RIM)

const unsigned char neighbours[TILES][4] =
  //  N  E  S  W
  { { 0, 1, 2, 3 }
  , { 0, 3, 2, 1 }
  , { 2, 3, 2, 3 }
  , { 0, 3, 0, 3 }
  , { 2, 3, 0, 1 }
  , { 2, 1, 0, 3 }
  , { 2, 1, 2, 1 }
  , { 0, 1, 0, 1 }
  };

const unsigned char subdivision[TILES][SUBDIVIDE][SUBDIVIDE] =
  { // 0
    { { 3, 0, 1, 2 }
    , { 0, 4, 5, 6 }
    , { 5, 1, 3, 5 }
    , { 7, 6, 1, 0 }
    }
    // 1
  , { { 3, 3, 0, 4 }
    , { 1, 0, 4, 3 }
    , { 6, 4, 3, 3 }
    , { 4, 0, 1, 0 }
    }
    // 2
  , { { 0, 4, 5, 4 }
    , { 2, 0, 1, 3 }
    , { 5, 4, 5, 1 }
    , { 7, 1, 0, 6 }
    }
    // 3
  , { { 0, 1, 3, 2 }
    , { 2, 5, 1, 2 }
    , { 5, 7, 6, 4 }
    , { 7, 7, 4, 0 }
    }
    // 4
  , { { 0, 4, 2, 2 }
    , { 4, 0, 4, 2 }
    , { 7, 6, 7, 4 }
    , { 7, 4, 3, 0 }
    }
    // 5
  , { { 0, 6, 4, 2 }
    , { 5, 4, 3, 5 }
    , { 0, 7, 1, 0 }
    , { 4, 3, 5, 6 }
    }
    // 6
  , { { 3, 5, 6, 4 }
    , { 7, 1, 4, 7 }
    , { 1, 2, 3, 0 }
    , { 4, 2, 0, 6 }
    }
    // 7
  , { { 0, 1, 3, 2 }
    , { 6, 4, 0, 6 }
    , { 4, 3, 5, 6 }
    , { 7, 7, 7, 6 }
    }
  };

float height[TILES][SIZE][SIZE];

double accum(int t, double y, double x, int d) {
  if (d > 0) {
    int j = floor(y);
    int i = floor(x);
    int tt = subdivision[t][(j * SUBDIVIDE) / SIZE][(i * SUBDIVIDE) / SIZE];
    return height[t][j][i] + accum(tt, fmod(y * SUBDIVIDE, SIZE), fmod(x * SUBDIVIDE, SIZE), d - 1) / SUBDIVIDE;
  } else {
    return 0;
  }
}


unsigned char pgm[SIZE][SIZE];

int main(int argc, char **argv) {
  srand(time(0));
  memset(height, 0, TILES * SIZE * SIZE * sizeof(float));
  for (int k = 0; k < COUNT; ++k) {
    // crater area is inversely proportional to frequency
    // this is hardcoded to 4x4 subdivision
    double p = (rand() + 1.0) / RAND_MAX; // 0  <  p <= 1
           p = exp2(fmod(-log2(p), 1.0)); // 1  <= p <  2
           p = 1.0 / (p * p);             // 1/4 < p <= 1
    double r = 0.5 * DIAMETER * p;
    double x = rand() / (double) RAND_MAX * SIZE;
    double y = rand() / (double) RAND_MAX * SIZE;
    int t0 = ((int) (rand() / (double) RAND_MAX * TILES)) % TILES;
    double r2 = r * r;
    // reject craters that cover edges
    if (x <= r || y <= r || SIZE - x <= r || SIZE - y < r) {
      continue;
    }
    // rasterize crater
    int lj = floor(y - r);
    int hj = ceil(y + r);
    int li = floor(x - r);
    int hi = ceil(x + r);
    #pragma omp parallel for
    for (int j = lj; j <= hj; ++j) {
      for (int i = li; i <= hi; ++i) {
        double dy = j - y;
        double dx = i - x;
        double dy2 = dy * dy;
        double dx2 = dx * dx;
        double d2 = dx2 + dy2;
        if (d2 < r2) {
          double z = sqrt(d2) / (BOWL * r);
          double h = 0;
          if (z < 1) {
            h = tanh(pow(z, 2) - 1) / 2 + 1 / RIM;
          } else {
            h = (sin(RIM * (z - 1)) + 1) / RIM;
          }
          h *= DEPTH * r;
          assert(0 <= j && j < SIZE);
          assert(0 <= i && i < SIZE);
          height[t0][j][i] += h;
        }
      }
    }
  }
  // rescale height to output pgm
  float mi =  1.0f / 0.0f;
  float ma = -1.0f / 0.0f;
  for (int t = 0; t < TILES; ++t) {
    for (int j = 0; j < SIZE; ++j) {
      for (int i = 0; i < SIZE; ++i) {
        mi = fminf(mi, height[t][j][i]);
        ma = fmaxf(ma, height[t][j][i]);
      }
    }
  }
  float k = 255.0 / (ma - mi);
  printf("min:  %f\nzero: %f\nmax:  %f\n", mi, k * (0 - mi), ma);
  char filename[] = "crater-X.pgm";
  for (int t = 0; t < TILES; ++t) {
    filename[7] = '0' + t;
    FILE *f = fopen(filename, "wb");
    fprintf(f, "P5\n%d %d\n255\n", SIZE, SIZE);
    #pragma omp parallel for
    for (int j = 0; j < SIZE; ++j) {
      for (int i = 0; i < SIZE; ++i) {
        pgm[j][i] = fminf(fmaxf(k * (height[t][j][i] - mi), 0), 255);
      }
    }
    fwrite(&pgm[0][0], SIZE * SIZE, 1, f);
    fclose(f);
  }
  // render an example using multiple levels
  // sum_i=0^inf 1/4^i = 4 / 3
  mi *= 4.0 / 3.0;
  ma *= 4.0 / 3.0;
  k = 255.0 / (ma - mi);
  FILE *f = fopen("craters.pgm", "wb");
  fprintf(f, "P5\n%d %d\n255\n", SIZE, SIZE);
  #pragma omp parallel for
  for (int j = 0; j < SIZE; ++j) {
    for (int i = 0; i < SIZE; ++i) {
      double h = 0;
      for (int jj = 0; jj < 4; ++jj) {
        for (int ii = 0; ii < 4; ++ii) {
          h += accum(0, j + jj / 4.0, i + ii / 4.0, 16);
        }
      }
      h /= 16.0;
      pgm[j][i] = fminf(fmaxf(k * (h - mi), 0), 255);
    }
  }
  fwrite(&pgm[0][0], SIZE * SIZE, 1, f);
  fclose(f);
  return 0;
}
