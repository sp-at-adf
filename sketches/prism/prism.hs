import Data.List (transpose)

type R = Double

type P = [R]

midpoint ps qs = map (/2) $ zipWith (+) ps qs

type N = [R]

magnitude = sqrt . sum . map (^2)

normalize ps = map (/ magnitude ps) ps

midnormal ps qs = normalize $ zipWith (+) ps qs

data V = V P N

midvertex (V p m) (V q n) = V (midpoint p q) (midnormal m n)

sphere (V p _) = V q q where q = normalize p

data T = T V V V

mapT f (T a b c) = T (f a) (f b) (f c)

subdivide (T a b c) =
  [ T a ab ca
  , T ab b bc
  , T ca bc c
  , T ab bc ca
  ]
  where
    ab = midvertex a b
    bc = midvertex b c
    ca = midvertex c a

subdivides = iterate (concatMap subdivide)

triangle a b c = T (V a n) (V b n) (V c n)
  where n = normalize $ (a `to` b) `cross` (a `to` c)

quad a b c d = [ triangle a b c, triangle c d a ]

to = zipWith subtract

cross [u1,u2,u3] [v1,v2,v3] = [u2*v3 - u3*v2, u3*v1 - u1*v3, u1*v2 - u2*v1 ]

prism l
  =  triangle a b c
  :  triangle d f e
  :  quad a c f d
  ++ quad c b e f
  ++ quad b a d e
  where
    a = [-l / 2, l / 2,  l * sqrt 3 / 6]
    b = [ l / 2, l / 2,  l * sqrt 3 / 6]
    c = [   0  , l / 2, -l * sqrt 3 / 2]
    d = [-l / 2,-l / 2,  l * sqrt 3 / 6]
    e = [ l / 2,-l / 2,  l * sqrt 3 / 6]
    f = [   0  ,-l / 2, -l * sqrt 3 / 2]

toList (T (V a p) (V b q) (V c r)) = [ a ++ b ++ c, p ++ q ++ r]

tables = foldr (zipWith (++)) (repeat []) . map toList

writeTable name xs = writeFile name (unlines $ map show xs)

main = do
  let volume = ((4 * pi / 3) / (sqrt 3 / 4)) ** (1 / 3)
      -- area = ((4 * pi) / (3 + sqrt 3 / 2)) ** (1 / 2)
      -- circum = sqrt (12 / 7)
      [position, normal] = tables $ subdivides (prism volume) !! 3
  writeTable "prism.position" position
  writeTable "prism.normal" normal
