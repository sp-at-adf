#version 120

uniform vec3 ambient;
uniform vec3 diffuse;
uniform vec3 specular;
uniform float shine;

uniform vec3 light;

varying vec4 position;
varying vec3 normal;

void main(void) {
  vec3 p = position.xyz/position.w;
  vec3 n = normalize(normal);
  vec3 l = normalize(light - p);
  float dlm = max(dot(l, n), 0.0);
  vec3 reflected = normalize(reflect(-l, n));
  float highlight = pow(max(reflected.z, 0.0), shine);
  gl_FragColor = vec4(mix(ambient + diffuse * dlm, specular, highlight), 1.0);
}
