#version 120

uniform float factor;

varying vec4 position;
varying vec3 normal;

void main(void) {
  vec3 p = gl_Vertex.xyz;
  vec3 n = gl_Normal.xyz;
  vec3 q = normalize(p);
  vec4 pos = gl_ModelViewProjectionMatrix * vec4(mix(p, q, factor), 1.0);
  normal = normalize(gl_NormalMatrix * vec3(normalize(mix(n, q, factor))));
  position = pos;
  gl_Position = pos;
}
