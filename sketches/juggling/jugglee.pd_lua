local J = pd.Class:new():register("jugglee")

function J:initialize()
  self.balls = { }
  self.currentBeat = 0
  self.currentHand = 1
  self.throwTime = 0.2
  self.catchTime = 0.2
  self.handRadius = 0.2
  self.inlets = 1
  self.outlets = 1
  return true
end

local function position(hands, ball, t)
  if (ball.t1 - ball.t0) == 1 then
    if     t < ball.t0 then
      return { 0, -1 }
    elseif t < ball.t1 then
      return { ball.x0 + (ball.x1 - ball.x0) * (t - ball.t0), -hands.handRadius }
    else
      return { 0, -1 }
    end
  else
    if     t < ball.t0 then
      return { 0, -1 }
    elseif t < ball.t0 + hands.throwTime then
      local d = 1
      if ball.x0 < 0 then d = -1 end
      local r = hands.handRadius
      local a = math.pi/2 * (t - ball.t0) / hands.throwTime
      return { ball.x0 + d * r * (1 - math.sin(a)), -r * math.cos(a) }
    elseif t < ball.t1 - hands.catchTime then
      local throw = (ball.t1 - hands.catchTime) - (ball.t0 + hands.throwTime)
      local dt = (t - (ball.t0 + hands.throwTime))
      return { ball.x0 + (ball.x1 - ball.x0) * dt / throw, throw * dt - dt * dt }
    elseif t < ball.t1 then
      local d = 1
      if ball.x1 < 0 then d = -1 end
      local r = hands.handRadius
      local a = math.pi/2 * (ball.t1 - t) / hands.catchTime
      return { ball.x1 - d * r * (1 - math.sin(a)), -r * math.cos(a) }    
    else
      return { 0, -1 }
    end
  end
end

function J:in_1_bang()
  self.currentBeat = self.currentBeat + 1
  self.currentHand = -self.currentHand
  local t = self.currentBeat + 0.5
  local balls = { }
  local i
  for i = 1,#(self.balls) do
    local ball = self.balls[i]
    local p = position(self, ball, t)
    if p[2] > 0 then
      table.insert(balls, ball)
    end
  end
  self.balls = balls
end

function J:in_1_float(phase)
  local t = self.currentBeat + phase
  local i
  for i = 1,#(self.balls) do
    local ball = self.balls[i]
    local p = position(self, ball, t)
    local x = p[1]
    local y = p[2]
    self:outlet(1, "ball", { ball.id, x, y })
  end
end

function J:in_1_throw(atoms)
  local id        = atoms[1]
  local direction = atoms[2]
  local speed     = atoms[3]
  local fhand = self.currentHand
  local thand = self.currentHand + 2 * direction
  local dx = -direction
  local ball = { }
  ball.id = id
  ball.x0 = fhand * (1 - self.handRadius)
  ball.x1 = thand * (1 + self.handRadius)
  ball.t0 = self.currentBeat
  ball.t1 = self.currentBeat + speed
  table.insert(self.balls, ball)
end

function J:in_1_pick() end
function J:in_1_drop() end
function J:in_1_wait() end
function J:in_1_pause() end
function J:in_1_remove() end
