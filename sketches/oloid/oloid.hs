import Data.Monoid (mconcat)

type R = Double

type V = (R, R, R)

norm2 (x, y, z) = x*x + y*y + z*z
norm x = sqrt (norm2 x)
midpoint = mix 0.5
mix k (x, y, z) (u, v, w) = (x * k + k' * u, y * k + k' * v, z * k + k' * w) where k' = 1 - k
distance (x, y, z) (u, v, w) = norm (x - u, y - v, z - w)

l = sqrt 3

-- ends of torse parameterized by angle
a t = (sin t, -1/2 - cos t, 0)
b t = (0, 1/2 + cos t, sin t)

-- angles on the same torse of length l satisfy
-- cos^2(u/2) cos^2(t/2) = 1/2^2
other t = 2 * acos (1 / (2 * cos (t / 2)))

-- midpoint of a torse
m t = midpoint (a t) (b (other t))

-- surface area
area = 4 * pi
triangleCount = 2 * 4 * 10 * 16
triangleArea = area / triangleCount

-- circumference through midpoint of torse
circumference = 4 * sum (zipWith distance ms (tail ms))
  where ms = [ m t | i <- [0 .. 1200], let t = 2 * pi * fromIntegral i / 3600 ]

-- parameterize circumference
c s = sum (zipWith distance ms (tail ms)) + (s' - fromIntegral (floor s')) * distance (m (fromIntegral (ceiling s') * 2 * pi / 3600)) (m (fromIntegral (floor s') * 2 * pi / 3600))
  where
    ms = [ m t | i <- [0 .. floor s'], let t = 2 * pi * fromIntegral i / 3600 ]
    s' = 3600 * s / (2 * pi)

-- binary search
search eps f target lo mid hi
  | abs (f mid - target) < eps = mid
  | f mid < target = search eps f target mid ((mid + hi)/2) hi
  | otherwise = search eps f target lo ((lo + mid)/2) mid

e = circumference / 180

ts = [ search e c t 0 (pi / 3) (2 * pi / 3) | i <- [30, 90 .. 1200], let t = 2 * pi * (fromIntegral i) / 3600 ]
us = map other ts

mesh = triangleStrip $ concat
  [ zip ts us
  , zip (reverse ts) (map negate $ reverse us)
  , zip (map negate ts) (map negate us)
  , zip (map negate $ reverse ts) (reverse us)
  ]

triangleStrip params@((t0, u0):_) = go params
  where
    go [] = a t0 : b u0 : []
    go ((t, u):tus) = a t : b u : go tus

triangles (a0:b0:ab@(a1:b1:_)) = subdivide a0 a1 b0 b1 ++ triangles ab
  where
    subdivide a0 a1 b0 b1
      | lab * l01 > 8 * triangleArea = subdivide a0 a1 c0 c1 ++ subdivide c0 c1 b0 b1
      | la == 0 = [(b0,a1,b1)]
      | lb == 0 = [(a0,a1,b0)]
      | otherwise = [(a0,a1,b0),(b0,b1,a1)]
      where
        lc = sqrt ((la * la + lb * lb) / 2)
        k = (la + lb) / (2 * (la + lc))
        c0 = mix k a0 b0
        c1 = mix k a1 b1
        l0 = distance a0 b0
        l1 = distance a1 b1
        la = distance a0 a1
        lb = distance b0 b1
        l01 = l0 + l1
        lab = la + lb
triangles _ = []

obj s = let ~(vs, fs) = mconcat $ go 1 s in unlines (vs ++ fs)
  where
    go _ [] = []
    go i ((a, b, c):abc) = ([v a, v b, v c], [t i]) : go (i + 3) abc
    v (x, y, z) = "v " ++ show x ++ " " ++ show y ++ " " ++ show z
    t i = "f " ++ show i ++ " " ++ show (i + 1) ++ " " ++ show (i + 2)

(xs, ys, zs) = unzip3 mesh

main = do
  writeFile "oloid.obj" . obj . triangles $ mesh
{-
  print (length mesh)
  writeFile "oloid-xs.txt" (unlines (map show xs))
  writeFile "oloid-ys.txt" (unlines (map show ys))
  writeFile "oloid-zs.txt" (unlines (map show zs))
  writeFile "oloid-rs.txt" (unlines (map show (map (\x -> 0.5 * x + 0.5) xs)))
  writeFile "oloid-gs.txt" (unlines (map show (map (\y -> y / 3 + 0.5) ys)))
  writeFile "oloid-bs.txt" (unlines (map show (map (\x -> 0.5 * x + 0.5) zs)))
  writeFile "oloid-as.txt" (unlines (map show (map (const 1) zs)))
-}
