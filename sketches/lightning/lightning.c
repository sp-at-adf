#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define width 256
#define height 256

int sky[height][width];

unsigned char ppm[height][width][3];

static inline int min(int i, int j) { return i < j ? i : j; }
static inline int max(int i, int j) { return i > j ? i : j; }
static inline int clamp(int i, int lo, int hi) { return min(max(i, lo), hi); }

int strike(int j, int i) {
  int n = sky[j][i];
  if (n > 0) {
    for (int jj = j - 1; jj <= j + 1; ++jj) {
      for (int ii = i - 1 + width; ii <= i + 1 + width; ++ii) {
        int iii = ii % width;
        int jjj = clamp(jj, 0, height-1);
        if (sky[jjj][iii] == n - 1) {
          if (strike(jjj, iii)) {
            ppm[jjj][iii][0] = 255;
            ppm[jjj][iii][1] = 255;
            return 1;
          }
        }
      }
    }
  } /*else if (n < -1) {
    for (int jj = j - 1; jj <= j + 1; ++jj) {
      for (int ii = i - 1 + width; ii <= i + 1 + width; ++ii) {
        int iii = ii % width;
        if (sky[jj][iii] == n + 1) {
          if (strike(jj, iii)) {
            ppm[jj][iii][0] = 255;
            ppm[jj][iii][1] = 255;
            return 1;
          }
        }
      }
    }
  } */else {
    return 1;
  }
  return 0;
}

int main(int argc, char **argv) {
  srand(time(0));
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      sky[j][i] = j == 0;
    }
  }
  int n, h = 0, i, j, k = 0, complete = 0;
  do {
    do {
      j = h + 1;
      i = rand() % width;
      do {
        j += rand() % 5 - 2;
        i += rand() % 5 - 2 + width;
        i %= width;
        k++;
        n = 0;
        j = clamp(j, 0, min(h + 4, height - 1));
        for (int jj = j - 1; jj <= j + 1; ++jj) {
          for (int ii = i - 1; ii <= i + 1; ++ii) {
            if (sky[clamp(jj, 0, height-1)][(ii + width) % width]) {
              if (n) {
                n = min(n, sky[clamp(jj, 0, height-1)][(ii + width) % width]);
              } else {
                n = sky[clamp(jj, 0, height-1)][(ii + width) % width];
              }
            }
          }
        }
      } while (! n);
    } while (! n);
/*
    int mi = INT_MAX;
    int ma = INT_MIN;
    for (int jj = j - 1; jj <= j + 1; ++jj) {
      for (int ii = i - 1; ii <= i + 1; ++ii) {
        //mi = min(mi, sky[clamp(jj, 0, height-1)][(ii + width) % width]);
        ma = max(ma, sky[clamp(jj, 0, height-1)][(ii + width) % width]);
      }
    }
*/
    sky[j][i] = n + 1;
/*
    if (mi < 0 && 0 < ma) {
    } else if (mi < 0) {
      sky[j][i] = mi - 1;
    } else if (0 < ma) {
      sky[j][i] = ma + 1;
    }
*/
    h = max(h, j);
    complete = h == height - 1;
  } while (! complete);
  memset(&ppm[0][0][0], 0, width * height * 3);
  strike(j, i);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      ppm[j][i][2] = sky[j][i] ? 255 : 0;
    }
  }
  printf("P6\n%d %d\n255\n", width, height);
  fwrite(&ppm[0][0][0], width * height * 3, 1, stdout);
  fprintf(stderr, "%d\n", k);
  return 0;
}
