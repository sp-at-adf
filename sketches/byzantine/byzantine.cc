#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/matrix_transform.hpp>

const int width = 640, height = 360;

const double twopi = 6.283185307179586;

const GLchar *vert_src =
  "#version 330 core\n"
  "uniform mat4 mvp;\n"
  "in vec3 vertex;\n"
  "out vec3 colour;\n"
  "void main() { vec4 v = mvp * vec4(vertex, 1.0); v.w += 0.5 - v.z; gl_Position = v; colour = vertex; }\n"
;

const GLchar *frag_src =
  "#version 330 core\n"
  "in vec3 colour;\n"
  "void main() { float edge = length(colour) > 1.25 ? 1.0 : 0.5; gl_FragData[0] = vec4((0.5 * colour + vec3(0.5)) * edge, 1.0); }\n"
;

typedef struct {
  GLuint vbo;
  GLuint vao;
  GLint mvp;
} G;

void init(G *g) {
  glGenBuffers(1, &g->vbo);
  glGenVertexArrays(1, &g->vao);
  glBindVertexArray(g->vao);
  glBindBuffer(GL_ARRAY_BUFFER, g->vbo);
  GLfloat cube[] =
    { -1, -1, -1,  -1, -1,  1,  -1,  1,  1
    , -1, -1, -1,  -1, -1,  1,   1, -1,  1
    , -1, -1, -1,  -1,  1, -1,  -1,  1,  1
    , -1, -1, -1,  -1,  1, -1,   1,  1, -1
    , -1, -1, -1,   1, -1, -1,   1, -1,  1
    , -1, -1, -1,   1, -1, -1,   1,  1, -1
    ,  1,  1,  1,  -1, -1,  1,  -1,  1,  1
    ,  1,  1,  1,  -1, -1,  1,   1, -1,  1
    ,  1,  1,  1,  -1,  1, -1,  -1,  1,  1
    ,  1,  1,  1,  -1,  1, -1,   1,  1, -1
    ,  1,  1,  1,   1, -1, -1,   1, -1,  1
    ,  1,  1,  1,   1, -1, -1,   1,  1, -1
    };
  glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);
  GLuint prog = glCreateProgram();
  GLuint vert = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vert, 1, &vert_src, 0);
  glCompileShader(vert);
  glAttachShader(prog, vert);
  GLuint frag = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(frag, 1, &frag_src, 0);
  glCompileShader(frag);
  glAttachShader(prog, frag);
  glLinkProgram(prog);
  glUseProgram(prog);
  g->mvp = glGetUniformLocation(prog, "mvp");
  glVertexAttribPointer(glGetAttribLocation(prog, "vertex"), 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(0);
  glEnable(GL_DEPTH_TEST);
}

glm::mat4 byzantine(float fov, float aspect, float near, float far) {
  float f = 1 / tan(fov / 2);
  float a = (far + near) / (near - far);
  float b = 2 * far * near / (near - far);
  float c = (a * (near + far) + 2 * b) / (near - far);
  float d = b * (near + far) - a * (near - far) / 2;
  float o = 0;
  return glm::mat4(f/aspect,o,o,o, o,f,o,o, o,o,a,b, o,o,c,d);
}

void display(G *g, int frame) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  for (float x = -1; x <= 1; x += 1) {
    glm::mat4 p = glm::perspective(float(90.0f * twopi/360.0f), 16.0f / 9.0f, 5.0f, 20.f);
    glm::mat4 t = glm::translate(glm::mat4(1.0f), glm::vec3(8.f * x, 0.0f, -8.f));
    glm::mat4 r = glm::rotate(glm::mat4(1.0f), float(frame * twopi/360), glm::vec3(1.f, 1.f, 0.f));
    glm::mat4 s = glm::scale(glm::mat4(1.0f), glm::vec3(2.0f));
    glm::mat4 m = p * t * r * s;
    glUniformMatrix4fv(g->mvp, 1, GL_FALSE, &m[0][0]);
    glDrawArrays(GL_TRIANGLES, 0, 36);
  }
}

void capture(unsigned char *b) {
  glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, b);
  printf("P6\n%d %d\n255\n", width, height);
  fflush(stdout);
  fwrite(b, width * height * 3, 1, stdout);
  fflush(stdout);
}

int main(int argc, char **argv) {
  (void) argc;
  (void) argv;
  G g;
  unsigned char b[width * height * 3];
  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(width, height, "byzantine", 0, 0);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError();
  init(&g);
  int frame = 0;
  while (! glfwWindowShouldClose(window)) {
    display(&g, frame++);
    glfwSwapBuffers(window);
    glfwPollEvents();
    int e = glGetError();
    if (e) fprintf(stderr, "%d\n", e);
//    capture(&b[0]);
//    if (frame == 360) { exit(0); }
  }
  return 0;
}
