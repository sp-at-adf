/* OpenGL example code - Tesselation
 * 
 * This example shows the usage of tesselation for terrain LOD.
 * The terrain is given as a texture of 3d samples (generalized
 * heightfield) and gets rendered without use of a vbo/vao. Instead
 * sample coordinates are generated from InstanceID and VertexID.
 * Tessellation is used to dynamically change the amount of vertices
 * depending on distance from the viewer.
 * This example requires at least OpenGL 4.0
 * 
 * Autor: Jakob Progsch
 */
 
/* index
 * line  122: vertex shader generates vertices from InstanceID/VertexID
 * line  139: tessellation control shader    
 * line  163: tessellation evaluation shader    
 * line  182: fragment shader with simple phong lighting        
 * line  202: shader compilation   
 * line  274: terrain generation
 * line  330: input handling        
 * line  415: draw call       
 */

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp> 

#include <iostream>
#include <string>
#include <vector>

const double pi = 3.141592653589793;

bool running;

// window close callback function
int closedWindow()
{
    running = false;
    return GL_TRUE;
}

// helper to check and display for shader compiler errors
bool check_shader_compile_status(GLuint obj)
{
    GLint status;
    glGetShaderiv(obj, GL_COMPILE_STATUS, &status);
    if(status == GL_FALSE)
    {
        GLint length;
        glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &length);
        std::vector<char> log(length);
        glGetShaderInfoLog(obj, length, &length, &log[0]);
        std::cerr << &log[0];
        return false;
    }
    return true;
}

// helper to check and display for shader linker error
bool check_program_link_status(GLuint obj)
{
    GLint status;
    glGetProgramiv(obj, GL_LINK_STATUS, &status);
    if(status == GL_FALSE)
    {
        GLint length;
        glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &length);
        std::vector<char> log(length);
        glGetProgramInfoLog(obj, length, &length, &log[0]);
        std::cerr << &log[0];
        return false;   
    }
    return true;
}

const GLfloat subdivision_data[8][4][4] =
  { // 0
    { { 3, 0, 1, 2 }
    , { 0, 4, 5, 6 }
    , { 5, 1, 3, 5 }
    , { 7, 6, 1, 0 }
    }
    // 1
  , { { 3, 3, 0, 4 }
    , { 1, 0, 4, 3 }
    , { 6, 4, 3, 3 }
    , { 4, 0, 1, 0 }
    }
    // 2
  , { { 0, 4, 5, 4 }
    , { 2, 0, 1, 3 }
    , { 5, 4, 5, 1 }
    , { 7, 1, 0, 6 }
    }
    // 3
  , { { 0, 1, 3, 2 }
    , { 2, 5, 1, 2 }
    , { 5, 7, 6, 4 }
    , { 7, 7, 4, 0 }
    }
    // 4
  , { { 0, 4, 2, 2 }
    , { 4, 0, 4, 2 }
    , { 7, 6, 7, 4 }
    , { 7, 4, 3, 0 }
    }
    // 5
  , { { 0, 6, 4, 2 }
    , { 5, 4, 3, 5 }
    , { 0, 7, 1, 0 }
    , { 4, 3, 5, 6 }
    }
    // 6
  , { { 3, 5, 6, 4 }
    , { 7, 1, 4, 7 }
    , { 1, 2, 3, 0 }
    , { 4, 2, 0, 6 }
    }
    // 7
  , { { 0, 1, 3, 2 }
    , { 6, 4, 0, 6 }
    , { 4, 3, 5, 6 }
    , { 7, 7, 7, 6 }
    }
  };

glm::vec3 hsv2rgb(glm::vec3 hsv)
{
  float h = hsv.x;
  float s = hsv.y;
  float v = hsv.z;
  float i, f, p, q, t, r, g, b;
  int ii;
  if (s == 0.0) {
    // Ignore hue
    r = v;
    g = v;
    b = v;
  } else {
    /* Apply physiological mapping: red, yellow, green and blue should
       be equidistant. */
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h); ii = (int) i;
    f = h - i;
    p = v*(1.0 - s);  // The "low-flat" curve
    q = v*(1.0 - (s*f));  // The "falling" curve
    t = v*(1.0 - (s*(1.0 - f)));  // The "rising" curve
    switch(ii) {
      case 0:  // red point
        r = v; g = t; b = p; // RED max, GRN rising, BLU min
        break;
      case 1:  // yellow point
        r = q; g = v; b = p; // RED falling, GRN max, BLU min
        break;
      case 2:  // green point
        r = p; g = v; b = t; // RED min, GRN max, BLU rising
        break;
      case 3:  // cyan point
        r = p; g = q; b = v; // RED min, GRN falling, BLU max
        break;
      case 4:  // blue point
        r = t; g = p; b = v; // RED rising, GRN min, BLU max
        break;
      case 5:  // magenta point
      default:
        r = v; g = p; b = q; // RED max, GRN min, BLU falling
        break;
    }
  }
  return glm::vec3(r, g, b);
}

void craters(std::vector<glm::vec4> &colourData, std::vector<GLfloat> &displacementData, int width, int height, int layers) {
  int subdivide = 4;
  double diameter = 256;
  double depth = 1.0 / 16;
  double density = 4;
  int count = density * subdivide * width * height / (diameter * diameter);
  double rim = 0.25;
  double bowl = 1 - rim;
  for (int k = 0; k < count; ++k) {
    // crater area is inversely proportional to frequency
    // this is hardcoded to 4x4 subdivision
    double p = (rand() + 1.0) / RAND_MAX; // 0  <  p <= 1
           p = exp2(fmod(-log2(p), 1.0)); // 1  <= p <  2
           p = 1.0 / (p * p);             // 1/4 < p <= 1
    GLfloat hue = rand() / (double) RAND_MAX;
    glm::vec4 colour = glm::vec4(hsv2rgb(GLfloat(p) * glm::vec3(hue, 1.0f, 1.0f)), GLfloat(p));
    double r = 0.5 * diameter * p;
    double x = rand() / (double) RAND_MAX * width;
    double y = rand() / (double) RAND_MAX * height;
    int t0 = ((int) (rand() / (double) RAND_MAX * layers)) % layers;
    double r2 = r * r;
    // reject craters that cover edges
    if (x <= r || y <= r || width - x <= r || height - y < r) {
      continue;
    }
    // rasterize crater
    int lj = floor(y - r);
    int hj = ceil(y + r);
    int li = floor(x - r);
    int hi = ceil(x + r);
    #pragma omp parallel for
    for (int j = lj; j <= hj; ++j) {
      for (int i = li; i <= hi; ++i) {
        double dy = j - y;
        double dx = i - x;
        double dy2 = dy * dy;
        double dx2 = dx * dx;
        double d2 = dx2 + dy2;
        if (d2 < r2) {
          double z = sqrt(d2) / r * (bowl + 4.0 * rim / 3.0);
          double h = 0;
          if (z < bowl) {
            h = tanh(2 * (pow(z / bowl, 2) - 1))/2;  // d/dz @ bowl = 2 / bowl
            h += (2 / bowl) / (3 * pi / (2 * rim));
          } else if (z < bowl + rim) {
            h = sin(1.5 * pi * (z - bowl) / rim) + 1; // d/dz @ bowl = 3 pi / (2 rim)
            h *= (2 / bowl) / (3 * pi / (2 * rim));
          }
          h *= -depth * p;
          assert(0 <= j && j < height);
          assert(0 <= i && i < width);
//          if (h < displacementData[i + j * width + t0 * width * height]) {
            displacementData[i + j * width + t0 * width * height] += h;
            float edge = fmin(fmax(0.01 / pow((z - bowl - -1.0/3.0 * rim) / (rim / 6.0), 8.0) - 1.0, 0.0), 1.0)
                       + fmin(fmax(0.01 / pow((z - bowl -  1.0/3.0 * rim) / (rim / 6.0), 8.0) - 1.0, 0.0), 1.0)
                       + fmin(fmax(0.01 / pow((z - bowl -  3.0/3.0 * rim) / (rim / 6.0), 8.0) - 1.0, 0.0), 1.0);
            colourData[i + j * width + t0 * width * height] += colour * glm::vec4(edge, edge, edge, 1.0);
//          }
        }
      }
    }
  }
}

int main()
{
    srand(time(0));
    int width = 1280;
    int height = 720;
    
    if(glfwInit() == GL_FALSE)
    {
        std::cerr << "failed to init GLFW" << std::endl;
        return 1;
    }

    // we need a 4.0 profile this time
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
 
    // create a window
    GLFWwindow *window = 0;
    if(!(window = glfwCreateWindow(width, height, "craterscape", 0, 0)))
    {
        std::cerr << "failed to open window" << std::endl;
        glfwTerminate();
        return 1;
    }

    glfwMakeContextCurrent(window);
    glewExperimental = GL_TRUE;
    glewInit();
    glGetError();

    glfwSwapInterval(1);
    
    // this time we disable the mouse cursor since we want differential
    // mouse input
//    glfwDisable(GLFW_MOUSE_CURSOR);

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

    // shader source code
    std::string vertex_source =
        "#version 400\n"
        "uniform uint width;\n"
        "uniform uint height;\n"
        "out vec4 tposition;\n"
        "const vec2 quad_offsets[6] = vec2[](\n"
        "   vec2(0,0),vec2(1,0),vec2(1,1),\n"
        "   vec2(0,0),vec2(1,1),vec2(0,1)\n"
        ");\n"
        "void main() {\n"
        "   vec2 base = vec2(gl_InstanceID%width, gl_InstanceID/width);\n"
        "   vec2 offset = quad_offsets[gl_VertexID];\n"
        "   vec2 pos = (base + offset)/vec2(width, height);\n"
        "   tposition = vec4(pos,0,1);\n"
        "}\n";    

    std::string tess_control_source =
        "#version 400\n"
        "uniform vec3 ViewPosition;\n"
        "uniform float tess_scale;\n"
        "layout(vertices = 3) out;\n"
        "in vec4 tposition[];\n"
        "out vec4 tcposition[];\n"
        "void main()\n"
        "{\n"
        "   tcposition[gl_InvocationID] = tposition[gl_InvocationID];\n"
        "   if(gl_InvocationID == 0) {\n"
        "       vec3 terrainpos = ViewPosition;\n"
        "       terrainpos.z -= clamp(terrainpos.z,-1.0, 1.0);\n"
        "       vec4 center = (tcposition[1]+tcposition[2])/2.0;\n"
        "       gl_TessLevelOuter[0] = min(16.0, 1+tess_scale*0.5/distance(center.xyz, terrainpos));\n"
        "       center = (tcposition[2]+tcposition[0])/2.0;\n"
        "       gl_TessLevelOuter[1] = min(16.0, 1+tess_scale*0.5/distance(center.xyz, terrainpos));\n"
        "       center = (tcposition[0]+tcposition[1])/2.0;\n"
        "       gl_TessLevelOuter[2] = min(16.0, 1+tess_scale*0.5/distance(center.xyz, terrainpos));\n"
        "       center = (tcposition[0]+tcposition[1]+tcposition[2])/3.0;\n"
        "       gl_TessLevelInner[0] = min(16.0, 1+tess_scale*0.7/distance(center.xyz, terrainpos));\n"
        "   }\n"
        "}\n";

    std::string tess_eval_source =
        "#version 400\n"
        "uniform vec2 scroll;\n"
        "uniform mat4 ViewProjection;\n"        
        "uniform sampler2DArray displacement;\n"
        "uniform sampler2DArray subdivision;\n"
        "layout(triangles, equal_spacing, cw) in;\n"
        "in vec4 tcposition[];\n"
        "out vec3 tecoord0;\n"
        "out vec4 teposition;\n"
        "void main()\n"
        "{\n"
        "   vec4 teposition0 = gl_TessCoord.x * tcposition[0]\n"
        "              + gl_TessCoord.y * tcposition[1]\n"
        "              + gl_TessCoord.z * tcposition[2];\n"
        "   vec3 tecoord = vec3(teposition0.xy + scroll, 0.0);\n"
        "   tecoord0 = tecoord;\n"
        "   vec3 offset = vec3(tecoord.xy, 0.0);\n"
        "   float scale = 1.0;\n"
        "   float factor = 4.0;\n"
        "   for (int i = 0; i < 2; ++i) {\n"
        "       offset.z += scale * texture(displacement, tecoord).x;\n"
        "       tecoord.z = texture(subdivision, tecoord).x;\n"
        "       tecoord.xy *= factor;\n"
        "       scale /= factor;\n"
        "   }\n"
        "   teposition0.xyz = offset;\n"
        "   teposition0.xy -= scroll;\n"
        "   teposition = teposition0;\n"
        "   gl_Position = ViewProjection*teposition0;\n"
        "}\n";
        
    std::string fragment_source =
        "#version 400\n"
        "uniform vec3 ViewPosition;\n"
        "uniform vec3 LightPosition;\n"
        "uniform sampler2DArray displacement;\n"
        "uniform sampler2DArray colour;\n"
        "uniform sampler2DArray subdivision;\n"
        "in vec4 teposition;\n"
        "in vec3 tecoord0;\n"
        "layout(location = 0) out vec4 FragColor;\n"
        "void main() {\n"
        "   vec4 c = vec4(0.0,0.0,0.0,0.1);\n"
        "   vec3 tecoord = tecoord0;\n"
        "   vec3 x = vec3(tecoord.xy, 0.0);\n"
        "   float scale = 1.0;\n"
        "   float factor = 4.0;\n"
        "   for (int i = 0; i < 2; ++i) {\n"
        "       float z = scale * texture(displacement, tecoord).x;\n"
        "       x.z =+ z;\n"
        "       c += scale * texture(colour, tecoord);\n"
//        "       t0.z += scale * textureOffset(displacement, tecoord, ivec2(1,0)).x;\n"
 //       "       t1.z += scale * textureOffset(displacement, tecoord, ivec2(0,1)).x;\n"
        "       tecoord.z = texture(subdivision, tecoord).x;\n"
        "       tecoord.xy *= factor;\n"
        "       scale /= factor;\n"
        "   }\n"
 //       "   vec3 normal = (gl_FrontFacing?1:-1)*normalize(cross(x-t0, x-t1));\n"
 //       "   vec3 light = -normalize(LightPosition-teposition.xyz);\n"
 //       "   vec3 reflected = reflect(normalize(ViewPosition-teposition.xyz), normal);\n"
 //       "   float ambient = 0.1 / distance(LightPosition, teposition.xyz);\n"
 //       "   float diffuse =  ambient * max(0,dot(normal, light));\n"
  //      "   float specular = ambient * pow(max(0,dot(reflected, light)), 64);\n"
        "   c.rgb /= c.a;"
  //      "   c.rgb *= 2.0 * (ambient + diffuse + specular);\n"
//        "   c.rgb -= min(min(c.r, c.g), c.b) * 0.5;"
//        "   c.rgb /= max(max(c.r, c.g), c.b);"
        "   float fog = clamp(1.0 - 2.0 * distance(ViewPosition, teposition.xyz), 0.0, 1.0);\n"
//        "   float fog = 1.0;\n"
        "   FragColor = vec4(mix(vec3(0.0), 2.0 * c.rgb, fog * fog), 1);\n"
        "}\n";

    // program and shader handles
    GLuint shader_program, vertex_shader, tess_control_shader, tess_eval_shader, fragment_shader;
    
    // we need these to properly pass the strings
    const char *source;
    int length;

    // create and compiler vertex shader
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    source = vertex_source.c_str();
    length = vertex_source.size();
    glShaderSource(vertex_shader, 1, &source, &length); 
    glCompileShader(vertex_shader);
    if(!check_shader_compile_status(vertex_shader))
    {
        return 1;
    }

    // create and compiler tesselation control shader
    tess_control_shader = glCreateShader(GL_TESS_CONTROL_SHADER);
    source = tess_control_source.c_str();
    length = tess_control_source.size();
    glShaderSource(tess_control_shader, 1, &source, &length); 
    glCompileShader(tess_control_shader);
    if(!check_shader_compile_status(tess_control_shader))
    {
        return 1;
    }
    
    // create and compiler tesselation evaluation shader
    tess_eval_shader = glCreateShader(GL_TESS_EVALUATION_SHADER);
    source = tess_eval_source.c_str();
    length = tess_eval_source.size();
    glShaderSource(tess_eval_shader, 1, &source, &length); 
    glCompileShader(tess_eval_shader);
    if(!check_shader_compile_status(tess_eval_shader))
    {
        return 1;
    }
 
    // create and compiler fragment shader
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    source = fragment_source.c_str();
    length = fragment_source.size();
    glShaderSource(fragment_shader, 1, &source, &length);   
    glCompileShader(fragment_shader);
    if(!check_shader_compile_status(fragment_shader))
    {
        return 1;
    }

    // create program
    shader_program = glCreateProgram();
    
    // attach shaders
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, tess_control_shader);
    glAttachShader(shader_program, tess_eval_shader);
    glAttachShader(shader_program, fragment_shader);
    
    // link the program and check for errors
    glLinkProgram(shader_program);
    check_program_link_status(shader_program);

    GLint width_Location = glGetUniformLocation(shader_program, "width");
    GLint height_Location = glGetUniformLocation(shader_program, "height");
    GLint ViewProjection_Location = glGetUniformLocation(shader_program, "ViewProjection");
    GLint ViewPosition_Location = glGetUniformLocation(shader_program, "ViewPosition");
    GLint LightPosition_Location = glGetUniformLocation(shader_program, "LightPosition");
    GLint displacement_Location = glGetUniformLocation(shader_program, "displacement");
    GLint colour_Location = glGetUniformLocation(shader_program, "colour");
    GLint subdivision_Location = glGetUniformLocation(shader_program, "subdivision");
    GLint tess_scale_Location = glGetUniformLocation(shader_program, "tess_scale");
    GLint scroll_Location = glGetUniformLocation(shader_program, "scroll");


    int terrainwidth = 1024, terrainheight = 1024;
    std::vector<GLfloat> displacementData(terrainwidth*terrainheight*8);
    std::vector<glm::vec4> colourData(terrainwidth*terrainheight*8);
    craters(colourData, displacementData, terrainwidth, terrainheight, 8);

     // texture handle
    GLuint displacement;
    
    // generate texture
    glGenTextures(1, &displacement);

    // bind the texture
    glBindTexture(GL_TEXTURE_2D_ARRAY, displacement);
    
    // set texture parameters
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    // set texture content
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, terrainwidth, terrainheight, 8, 0, GL_RED, GL_FLOAT, &displacementData[0]);
    glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

     // texture handle
    GLuint colour;
    
    // generate texture
    glGenTextures(1, &colour);

    // bind the texture
    glBindTexture(GL_TEXTURE_2D_ARRAY, colour);
    
    // set texture parameters
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
    
    // set texture content
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_RGBA32F, terrainwidth, terrainheight, 8, 0, GL_RGBA, GL_FLOAT, &colourData[0]);
    glGenerateMipmap(GL_TEXTURE_2D_ARRAY);

    GLuint subdivision;
    glGenTextures(1, &subdivision);
    glBindTexture(GL_TEXTURE_2D_ARRAY, subdivision);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, 4, 4, 8, 0, GL_RED, GL_FLOAT, &subdivision_data[0][0][0]);

    // camera position and orientation
    glm::vec3 position(0.5, 0.5, -0.04);
    glm::mat4 rotation = glm::mat4(1.0f);
    rotation = glm::rotate(rotation, glm::radians(90.0f), glm::vec3(1.0f,0.0f,0.0f));
    glm::mat3 rotation3(rotation);
    glm::vec3 up = glm::transpose(rotation3)*glm::vec3(0.0f, 1.0f, 0.0f);
    GLfloat phi = (sqrt(5) + 1)/2;
    rotation = glm::rotate(rotation, atan2f(phi, 1.0f), up);
//    rotation = glm::rotate(rotation, 90.f, up);
//    float t = glfwGetTime();
    bool tessellation = true;
    bool space_down = false;
    
    glEnable(GL_DEPTH_TEST);
    
    // check for errors
    GLenum error = glGetError();
    if(error != GL_NO_ERROR)
    {
        std::cerr << "preloop: "<< gluErrorString(error);
        return 1;
    }
//    std::vector<unsigned char> buffer(width * height * 3);
    while(! glfwWindowShouldClose(window))
    {    
           // calculate timestep
        float dt = 0.04;
/*
        float new_t = glfwGetTime();
        float dt = new_t - t;
        t = new_t;
        std::cerr << dt << "\r";
*/

        // find up, forward and right vector
        glm::mat3 rotation3(rotation);
//        glm::vec3 up = glm::transpose(rotation3)*glm::vec3(0.0f, 1.0f, 0.0f);
        glm::vec3 right = glm::transpose(rotation3)*glm::vec3(1.0f, 0.0f, 0.0f);
        glm::vec3 forward = glm::transpose(rotation3)*glm::vec3(0.0f, 0.0f,-1.0f);
        
        // apply mouse rotation
//        rotation = glm::rotate(rotation,  0.2f*mousediff.x, up);
//        rotation = glm::rotate(rotation,  0.2f*mousediff.y, right);
//        rotation = glm::rotate(rotation,  0.01f, up);
      
        // roll
        if(glfwGetKey(window, 'Q'))
        {
            rotation = glm::rotate(rotation, glm::radians(180.0f*dt), forward);
        }  
        if(glfwGetKey(window, 'E'))
        {
            rotation = glm::rotate(rotation,-glm::radians(180.0f*dt), forward);
        }
        
        float speed = 0.1f;
        // movement
//        if(glfwGetKey('W'))
        {
            position += speed*dt*forward; 
        }  
        if(glfwGetKey(window, 'S'))
        {
            position -= speed*dt*forward;
        }
        if(glfwGetKey(window, 'D'))
        {
            position += speed*dt*right; 
        }  
        if(glfwGetKey(window, 'A'))
        {
            position -= speed*dt*right;
        }
        
        // terminate on escape 
        if(glfwGetKey(window, GLFW_KEY_ESCAPE))
        {
            break;
        }
        
        // toggle tesselation
        if(glfwGetKey(window, GLFW_KEY_SPACE) && !space_down)
        {
            tessellation = !tessellation;
        }
        space_down = glfwGetKey(window, GLFW_KEY_SPACE);
        
        // calculate ViewProjection matrix
        glm::vec2 scroll(round(position.x * 1024)/1024, round(position.y * 1024)/1024);
        glm::mat4 Projection = glm::perspective(glm::radians(45.0f), float(width) / height, 0.001f, 10.f);
        glm::mat4 View = glm::rotate(rotation, glm::radians(15.0f), right)*glm::translate(glm::mat4(1.0f), glm::vec3(-0.5 - position.x + scroll.x,-0.5 - position.y + scroll.y,-position.z));
        glm::mat4 ViewProjection = Projection*View;
        
        // clear first
        glClearColor(0,0,0,1);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D_ARRAY, displacement);
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D_ARRAY, colour);
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D_ARRAY, subdivision);
        
        // use the shader program
        int gridsize = 64;
        glUseProgram(shader_program);
        glUniform1ui(width_Location, gridsize); // 64x64 base grid without tessellation
        glUniform1ui(height_Location, gridsize);
        glUniformMatrix4fv(ViewProjection_Location, 1, GL_FALSE, glm::value_ptr(ViewProjection));
        glUniform3f(ViewPosition_Location, 0.5, 0.5, position.z);
        glUniform3f(LightPosition_Location, 0.5, 0.5, -0.01);
        glUniform2f(scroll_Location, scroll.x, scroll.y);
        
        if(tessellation)
            glUniform1f(tess_scale_Location, 5.0f);
        else
            glUniform1f(tess_scale_Location, 0.0f);
        
        // set texture uniform
        glUniform1i(displacement_Location, 0);
        glUniform1i(colour_Location, 1);
        glUniform1i(subdivision_Location, 2);
        
        // draw
        glPatchParameteri(GL_PATCH_VERTICES, 3);
        glDrawArraysInstanced(GL_PATCHES, 0, 6, gridsize * gridsize);
        
        // check for errors
        GLenum error = glGetError();
        if(error != GL_NO_ERROR)
        {
            std::cerr << gluErrorString(error);
            break;
        }
        
        // finally swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
/*
        glReadPixels(0,0,width,height,GL_RGB,GL_UNSIGNED_BYTE,&buffer[0]);
        fprintf(stdout, "P6\n%d %d\n255\n", width, height);
        fwrite(&buffer[0], width *height * 3, 1, stdout);
*/
    }
    // delete the created objects

    glDeleteVertexArrays(1, &vao);
    glDetachShader(shader_program, vertex_shader);
    glDetachShader(shader_program, tess_control_shader);
    glDetachShader(shader_program, tess_eval_shader);
    glDetachShader(shader_program, fragment_shader);
    glDeleteShader(vertex_shader);
    glDeleteShader(tess_control_shader);
    glDeleteShader(tess_eval_shader);
    glDeleteShader(fragment_shader);
    glDeleteProgram(shader_program);
    
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}

