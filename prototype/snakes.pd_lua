local S = pd.Class:new():register("snakes")

function S:initialize(name, atoms)
  self.snakes = { }
  self.inlets = 2
  self.outlets = 3
  self.behaviour = "wriggle"
  self.maxheight = 4
  self.other = { head = "tail", tail = "head" }
  return true
end

function S:in_1_bang()
  local i, s, sel, msg
  for i,s in ipairs(self.snakes) do
    self:outlet(2, "head", s.head)
    self:outlet(2, "tail", s.tail)
    self:outlet(1, "bang", {})
  end
end

function S:get_target(head)
  local a = math.random() * 2.0 * math.pi
  local x = head[1] + 2 * math.cos(a)
  local y = head[2] + 2 * math.sin(a)
  if x * x + y * y < 40 * 40 then return { x, y, 0 } else return self:get_target(head) end
end

function S:in_2_tick(atoms)
  local dt = atoms[1]
  local i, s, a, f, dx, dy, dz
  for i,s in ipairs(self.snakes) do
    s.phase = s.phase + dt
    if s.phase > 1 then
      s.phase = s.phase - 1 - 0.1 * math.random()
      self:outlet(3, "list", { i, s.target[1], s.target[2], s.target[3] })
      a = math.random() * 2.0 * math.pi
      if self.behaviour == "wriggle" or (self.behaviour == "column" and s.move[1] == "head") then
        if s.move[1] == "tail" then
          s.source[1] = s.head[1]
          s.source[2] = s.head[2]
          s.source[3] = s.head[3]
          s.target = { 0.5 * (s.tail[1] + s.head[1]), 0.5 * (s.tail[2] + s.head[2]), 0 }
        else
          s.source[1] = s.tail[1]
          s.source[2] = s.tail[2]
          s.source[3] = s.tail[3]
          s.target = self:get_target(s.head)
        end
        s.move[1] = self.other[s.move[1]]
      else if self.behaviour == "column" then
        s.source[1] = s.target[1]
        s.source[2] = s.target[2]
        s.source[3] = s.target[3]
        s.move[1] = "tail"
        s.target[1] = s.head[1]
        s.target[2] = s.head[2]
        s.target[3] = math.min(s.target[3] + 0.25, self.maxheight)
      end end
    else
      f = 1 - math.exp(-5 * math.max(s.phase, 0))
      s[s.move[1]][1] = s.source[1] * (1 - f) + f * s.target[1]
      s[s.move[1]][2] = s.source[2] * (1 - f) + f * s.target[2]
      s[s.move[1]][3] = s.source[3] * (1 - f) + f * s.target[3]
    end
  end
end

function S:in_2_reset(atoms)
  local i, x, y, z, b, n, t, d
  if "number" == type(atoms[1]) then n = atoms[1] else n = 10 end
  r = math.sqrt(n)
  self.snakes = { }
  for i = 1,n do
    a = math.random() * 2.0 * math.pi
    d = math.random() * 40
    x = d * math.cos(a)
    y = d * math.sin(a)
    z = 0
    a = math.random() * 2.0 * math.pi
    b = math.random() * 2.0 * math.pi
    t = math.random()
    self.snakes[i] =
      { head = { x, y, z }
      , tail = { x + math.cos(a), y + math.sin(a), z }
      , move = { "tail" }
      , source = { x + math.cos(a), y + math.sin(a), z }
      , target = { x + 2 * math.cos(b), y + 2 * math.sin(b), z }
      , phase = t
      }
  end
end

function S:in_2_wriggle() self.behaviour = "wriggle" end
function S:in_2_column() self.behaviour = "column" end
function S:in_2_height(atoms) self.maxheight = 6 * atoms[1] end
