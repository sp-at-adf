#!/bin/bash
stem="zoon-$(date --iso)"
avconv -i ../sketches/thunder/o.wav -i o%05d.tif -vf vflip -s 384x288 -vb 1M -ab 192k -acodec libvorbis "${stem}.ogv"
avconv -i ../sketches/thunder/o.wav -i o%05d.tif -vf vflip -target dvd -aspect 4:3 "${stem}.mpeg"
