set terminal pngcairo enhanced size 1024,1024
unset key
unset tics
unset border
unset colorbox
set bmargin at screen 0
set lmargin at screen 0
set rmargin at screen 1
set tmargin at screen 1
set parametric
set samples 100
set isosamples 50,25
set palette rgb 4,5,6
#set style line 1 lc rgb '#000000' lt 1 lw 1
set pm3d depthorder
set hidden3d
set style fill transparent solid 0.5
set view 30,180
max(a,b)=a>b?a:b
s(t) = 2*pi/3 * sgn(t) *(3*abs(t)-2*abs(t)**1.5)
ax(t)=sin(s(t))
ay(t)=-cos(s(t))
az(t)=0
bx(t)=0
by(t)=1/(1+cos(s(t)))
bz(t)=sqrt(1+2*cos(s(t)))/(1+cos(s(t)))
l(u,v)=sqrt(ox(u,v)**2 + (oy(u,v))**2 + oz(u,v)**2)
ox(u,v)=ax(u)*(1-v)+v*bx(u)
oy(u,v)=ay(u)*(1-v)+v*by(u) - 0.5
oz(u,v)=az(u)*(1-v)+v*bz(u)
px(u,v)=ox(u,v)/l(u,v)
py(u,v)=(oy(u,v))/l(u,v)
pz(u,v)=oz(u,v)/l(u,v)
m(u,v)=max(max(abs(px(u,v)),abs(py(u,v))),abs(pz(u,v)))* (8.0/3.052418468)**(1.0/3.0)
qx(u,v)=px(u,v)/m(u,v)
qy(u,v)=py(u,v)/m(u,v)
qz(u,v)=pz(u,v)/m(u,v)
rx(u,v)=ox(u,v)*(1-w)+w*qx(u,v)
ry(u,v)=oy(u,v)*(1-w)+w*qy(u,v)
rz(u,v)=oz(u,v)*(1-w)+w*qz(u,v)
x(u,v)=rx(u,v)*cos(phi)-sin(phi)*ry(u,v)
y(u,v)=rx(u,v)*sin(phi)+cos(phi)*ry(u,v)
z(u,v)=rz(u,v)
set xrange [-1.6:1.6]
set yrange [-1.6:1.6]
set zrange [-1.6:1.6]
phi=-pi/4
theta=atan2(1,sqrt(2))
do for [f=0:100] {
  set output sprintf('oloid-%03d.png',f)
  w=f/100.0
  splot [-1:1][0:1] \
  x(u,v),y(u,v)*cos(theta)-sin(theta)*z(u,v),y(u,v)*sin(theta)+cos(theta)*z(u,v) w pm3d, \
  x(u,v),y(u,v)*cos(theta)+sin(theta)*z(u,v),y(u,v)*sin(theta)-cos(theta)*z(u,v) w pm3d
}
