<http://www.rit.edu/cos/colorscience/rc_munsell_renotation.php>
<http://www.rit-mcsl.org/MunsellRenotation/real_sRGB.xls>
cat real_sRGB.csv |
sed 's|.*,\(.*\),\(.*\),\(.*\),.*,.*,.*,.*,.*,.*,.*,.*,.*,\(.*\),\(.*\),\(.*\),.*,.*,.*|\1 \2 \3 \4 \5 \6|' |
sed -e 's|YR| 2|' -e 's|GY| 4|' -e 's|BG| 6|' -e 's|PB| 8|' -e 's|RP| 10|' -e 's|R| 1|' -e 's|Y| 3|' -e 's|G| 5|' -e 's|B| 7|' -e 's|P| 9|' |
ghc -e 'interact (unlines . map unwords . map (map show) . (\xs -> [ [(b -1) * 100.0 + a, c, d, e, f, g] | [a,b,c,d,e,f,g] <- xs ]) . map (map read) . map words . lines)' > hvs2srgb.txt

./cuboloid | ffmpeg -f rawvideo -pix_fmt yuv420p -s:v 640x360 -r 25 -i - -vb 2M output.ogv

mkfifo test.yuv
./cuboloid > test.yuv &
x264 --demuxer lavf --input-csp yuv420p --input-range tv --colorprim bt709 --transfer bt709 --colormatrix bt709 --sar 1:1 --fps 25 --profile high --bluray-compat --crf 12 --input-res 640x360 -o output.mkv test.yuv

./cuboloid | ffmpeg -f rawvideo -pix_fmt yuv420p -s:v 640x360 -r 25 -i - -pass 1 -c:v libx264 -b:v 760k -bt 760k -y output1.mkv
./cuboloid | ffmpeg -f rawvideo -pix_fmt yuv420p -s:v 640x360 -r 25 -i - -pass 2 -c:v libx264 -b:v 760k -bt 760k -y output.mkv


[libx264 @ 0x15b0060] frame MB size (120x75) > level limit (8192)
[libx264 @ 0x15b0060] DPB size (4 frames, 36000 mbs) > level limit (3 frames, 32768 mbs)
[libx264 @ 0x15b0060] MB rate (540000) > level limit (245760)
[libx264 @ 0x15b0060] using cpu capabilities: MMX2 SSE2Fast LZCNT
[libx264 @ 0x15b0060] profile High, level 4.0
[libx264 @ 0x15b0060] 264 - core 142 r2431 a5831aa - H.264/MPEG-4 AVC codec - Copyleft 2003-2014 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x3:0x113 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=1 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=-2 threads=6 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=18.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00

[libx264 @ 0x21f6060] frame MB size (120x75) > level limit (8192)
[libx264 @ 0x21f6060] DPB size (4 frames, 36000 mbs) > level limit (3 frames, 32768 mbs)
[libx264 @ 0x21f6060] MB rate (540000) > level limit (245760)
[libx264 @ 0x21f6060] using cpu capabilities: MMX2 SSE2Fast LZCNT
[libx264 @ 0x21f6060] profile High, level 4.1
[libx264 @ 0x21f6060] 264 - core 142 r2431 a5831aa - H.264/MPEG-4 AVC codec - Copyleft 2003-2014 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x3:0x113 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=1 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=-2 threads=6 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=18.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00

[libx264 @ 0x116b060] frame MB size (120x75) > level limit (8704)
[libx264 @ 0x116b060] DPB size (4 frames, 36000 mbs) > level limit (3 frames, 34816 mbs)
[libx264 @ 0x116b060] MB rate (540000) > level limit (522240)
[libx264 @ 0x116b060] using cpu capabilities: MMX2 SSE2Fast LZCNT
[libx264 @ 0x116b060] profile High, level 4.2
[libx264 @ 0x116b060] 264 - core 142 r2431 a5831aa - H.264/MPEG-4 AVC codec - Copyleft 2003-2014 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x3:0x113 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=1 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=-2 threads=6 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=18.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00

[libx264 @ 0x1162060] frame MB size (120x75) > level limit (8192)
[libx264 @ 0x1162060] DPB size (4 frames, 36000 mbs) > level limit (3 frames, 32768 mbs)
[libx264 @ 0x1162060] MB rate (540000) > level limit (245760)
[libx264 @ 0x1162060] using cpu capabilities: MMX2 SSE2Fast LZCNT
[libx264 @ 0x1162060] profile High 4:4:4 Predictive, level 4.0, 4:4:4 8-bit
[libx264 @ 0x1162060] 264 - core 142 r2431 a5831aa - H.264/MPEG-4 AVC codec - Copyleft 2003-2014 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x3:0x113 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=1 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=4 threads=6 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=18.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00

[libx264 @ 0xc41060] frame MB size (120x75) > level limit (8192)
[libx264 @ 0xc41060] DPB size (4 frames, 36000 mbs) > level limit (3 frames, 32768 mbs)
[libx264 @ 0xc41060] MB rate (540000) > level limit (245760)
[libx264 @ 0xc41060] using cpu capabilities: MMX2 SSE2Fast LZCNT
[libx264 @ 0xc41060] profile High 4:4:4 Predictive, level 4.1, 4:4:4 8-bit
[libx264 @ 0xc41060] 264 - core 142 r2431 a5831aa - H.264/MPEG-4 AVC codec - Copyleft 2003-2014 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x3:0x113 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=1 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=4 threads=6 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=18.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00

[libx264 @ 0x14cd060] frame MB size (120x75) > level limit (8704)
[libx264 @ 0x14cd060] DPB size (4 frames, 36000 mbs) > level limit (3 frames, 34816 mbs)
[libx264 @ 0x14cd060] MB rate (540000) > level limit (522240)
[libx264 @ 0x14cd060] using cpu capabilities: MMX2 SSE2Fast LZCNT
[libx264 @ 0x14cd060] profile High 4:4:4 Predictive, level 4.2, 4:4:4 8-bit
[libx264 @ 0x14cd060] 264 - core 142 r2431 a5831aa - H.264/MPEG-4 AVC codec - Copyleft 2003-2014 - http://www.videolan.org/x264.html - options: cabac=1 ref=3 deblock=1:0:0 analyse=0x3:0x113 me=hex subme=7 psy=1 psy_rd=1.00:0.00 mixed_ref=1 me_range=16 chroma_me=1 trellis=1 8x8dct=1 cqm=0 deadzone=21,11 fast_pskip=1 chroma_qp_offset=4 threads=6 lookahead_threads=1 sliced_threads=0 nr=0 decimate=1 interlaced=0 bluray_compat=0 constrained_intra=0 bframes=3 b_pyramid=2 b_adapt=1 b_bias=0 direct=1 weightb=1 open_gop=0 weightp=2 keyint=250 keyint_min=25 scenecut=40 intra_refresh=0 rc_lookahead=40 rc=crf mbtree=1 crf=18.0 qcomp=0.60 qpmin=0 qpmax=69 qpstep=4 ip_ratio=1.40 aq=1:1.00


ffmpeg -i 2400f.wav -b:a 256k -strict experimental 1920x1200p60x1e.aac
ffmpeg -i 2395f.wav -b:a 256k -strict experimental 1920x1200p60x25.aac
