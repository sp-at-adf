#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define twopi 6.283185307179586
#define maxcount 2000

struct { float h, s, v, r, g, b; } c[maxcount];

int main(int argc, char **argv) {
  int count = 0;
  for (int i = 0; i < maxcount; ++i) {
    count = i;
    if (6 != scanf("%f %f %f %f %f %f\n", &c[i].h, &c[i].v, &c[i].s, &c[i].r, &c[i].g, &c[i].b)) { break; };
    c[i].h /= 100.0f;
  }
  int fs = 499 * 60;
  for (int f = 0; f < fs; ++f) {
    float x = f / (float) fs;
    float h = 10 * fmod(8 * x, 1);
    float s = 15 - 5 * cos(twopi * 7 * x);
    float v = 8;
    float r = 0;
    float g = 0;
    float b = 0;
    float a = 0;
    for (int i = 0; i < count; ++i) {
      float dh = fminf(fabsf(c[i].h - h), 10.0f - fabsf(c[i].h - h));
      float ds = c[i].s - s;
      float dv = c[i].v - v;
      float d = dh * dh + ds * ds + dv * dv;
      float w = exp(-d / 4.0);
      r += c[i].r * w;
      g += c[i].g * w;
      b += c[i].b * w;
      a += w;
    }
    r /= a;
    g /= a;
    b /= a;
    printf("%f %f %f\n", r, g, b);
  }
  return 0;
}
