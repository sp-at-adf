#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <stdio.h>
#include <stdlib.h>

// linear interpolation by Robert Munafo mrob.com
static float lin_interp
  ( float x
  , float domain_low
  , float domain_hi
  , float range_low
  , float range_hi
  ) {
  if ((x >= domain_low) && (x <= domain_hi)) {
    x = (x - domain_low) / (domain_hi - domain_low);
    x = range_low + x * (range_hi - range_low);
  }
  return x;
}

// perceptually balanced hue adjustment by Robert Munafo mrob.com
static float pvp_adjust_3(float x) {
  x = lin_interp(x, 0.00, 0.125, -0.050, 0.090);
  x = lin_interp(x, 0.125, 0.25,  0.090, 0.167);
  x = lin_interp(x, 0.25, 0.375,  0.167, 0.253);
  x = lin_interp(x, 0.375, 0.50,  0.253, 0.383);
  x = lin_interp(x, 0.50, 0.625,  0.383, 0.500);
  x = lin_interp(x, 0.625, 0.75,  0.500, 0.667);
  x = lin_interp(x, 0.75, 0.875,  0.667, 0.800);
  x = lin_interp(x, 0.875, 1.00,  0.800, 0.950);
  return(x);
}

static void hsv2rgb
  ( float h
  , float s
  , float v
  , float *rp
  , float *gp
  , float *bp
  ) {
  float i, f, p, q, t, r, g, b;
  if (s == 0.0) {
    r = v;
    g = v;
    b = v;
  } else {
    h = pvp_adjust_3(h);
    h = h - floor(h);
    h = h * 6.0;
    i = floor(h);
    f = h - i;
    p = v*(1.0 - s);
    q = v*(1.0 - (s*f));
    t = v*(1.0 - (s*(1.0 - f)));
    if (i < 1.0) { r = v; g = t; b = p; } else
    if (i < 2.0) { r = q; g = v; b = p; } else
    if (i < 3.0) { r = p; g = v; b = t; } else
    if (i < 4.0) { r = p; g = q; b = v; } else
    if (i < 5.0) { r = t; g = p; b = v; } else
                 { r = v; g = p; b = q; }
  }
  *rp = r;
  *gp = g;
  *bp = b;
}

static const double pi = 3.141592653589793;
static const double twopi = 6.283185307179586;

static const char *morph_vertex = R"(
#version 410 core
layout(location = 0) in vec3 oloid;
layout(location = 1) in vec3 cube;
uniform float morph;
void main(void) {
  gl_Position = vec4(mix(oloid, cube /  pow(8.0/3.052418468, 1.0/3.0), morph), 1.0);
}
)";

static const char *morph_geometry = R"(
#version 410 core
layout(triangles) in;
layout(triangle_strip, max_vertices = 3) out;
out vec3 surface;
out vec3 normal;
uniform mat4 modelMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;
void main(void) {
  vec3 d1 = gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz;
  vec3 d2 = gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz;
  vec3 n = normalMatrix * -normalize(cross(normalize(d1), normalize(d2)));
  vec4 p0 = modelMatrix * gl_in[0].gl_Position;
  vec4 p1 = modelMatrix * gl_in[1].gl_Position;
  vec4 p2 = modelMatrix * gl_in[2].gl_Position;
  p0.w *= 3.0 / -p0.z;
  p1.w *= 3.0 / -p1.z;
  p2.w *= 3.0 / -p2.z;
  vec3 s0 = p0.xyz / p0.w;
  vec3 s1 = p1.xyz / p1.w;
  vec3 s2 = p2.xyz / p2.w;
  p0 = projectionMatrix * p0;
  p1 = projectionMatrix * p1;
  p2 = projectionMatrix * p2;
  surface = s0;
  normal = n;
  gl_Position = p0;
  EmitVertex();
  surface = s1;
  normal = n;
  gl_Position = p1;
  EmitVertex();
  surface = s2;
  normal = n;
  gl_Position = p2;
  EmitVertex();
  EndPrimitive();
}
)";

// <https://en.wikipedia.org/wiki/Specular_highlight#Cook.E2.80.93Torrance_model>
// <http://inst.eecs.berkeley.edu/~cs283/sp13/lectures/cookpaper.pdf>
static const char *morph_fragment = R"(
#version 410 core
layout(location = 0, index = 0) out vec4 colour;
const float pi = 3.141592653589793;
uniform vec3 eye;
uniform vec3 material;
uniform float ambient;
uniform float diffuse;
uniform float specular;
uniform float index;
uniform float roughness;
uniform float alpha;
uniform float scale;
in vec3 surface;
in vec3 normal;
void main(void) {
  vec3 V = normalize(eye - surface);
  V.xy *= -1.0;
  vec3 N = normalize(normal);
  float diff = ambient;
  float spec = 0.0;
  float q = 1.0;
  float vn = dot(V, N);
  if (vn < 0.0) {
    discard;
  } else {
    for (int i = -1; i <= 1; i += 1) {
      for (int j = -1; j <= 1; j += 1) {
        vec3 light = scale * 32.0 * vec3(float(i), float(j), 0.0);
        vec3 L = normalize(light - surface);
        vec3 H = normalize(L + V);
        float k = 0.0;
        float ln = max(0.0, dot(L, N));
        if (ln > 0.0) {
        float vh = max(0.0, dot(V, H));
        float hn = max(0.0, dot(H, N));
        float d = hn * hn;
        float D = exp((d - 1.0) / (d * roughness)) / (pi * roughness * d * d);
        float F;
        {
          float c = vh;
          float g = sqrt(index * index + c * c - 1.0);
          float a0 = g - c;
          float a1 = g + c;
          float a2 = a1 * c - 1.0; a2 *= a2;
          float a3 = a0 * c - 1.0; a3 *= a3;
          float a4 = 1.0 + a2 / a3;
          a0 *= a0;
          a1 *= a1;
          F = 0.5 * a0 / a1 * a4;
        }
        float g = 2.0 * hn / vh;
        float G = min(1.0, min(g * vn, g * ln));
        k = D * F * G / (4.0 * vn * ln);
        }
        diff += diffuse * ln;
        spec += specular * k;
      }
    }
    colour =  vec4(clamp(q * 2.0 * material * diff + 2.0 * alpha * vec3(spec), vec3(0.0), vec3(1.0)), alpha);
  }
}
)";

static const char *median_vertex = R"(
#version 410 core
out vec2 c;
void main(void) {
  switch (gl_VertexID) {
  case 0:
    c = vec2(0.0, 0.0);
    gl_Position = vec4(-1.0, -1.0, 0.0, 1.0);
    break;
  case 1:
    c = vec2(1.0, 0.0);
    gl_Position = vec4( 1.0, -1.0, 0.0, 1.0);
    break;
  case 2:
    c = vec2(0.0, 1.0);
    gl_Position = vec4(-1.0,  1.0, 0.0, 1.0);
    break;
  case 3:
    c = vec2(1.0, 1.0);
    gl_Position = vec4( 1.0,  1.0, 0.0, 1.0);
    break;
  }
}
)";

// bubble sort
static const char *median_fragment = R"(
#version 410 core
layout(location = 0, index = 0) out vec4 colour;
uniform sampler2D t;
uniform vec2 d;
uniform bool median;
in vec2 c;
void main(void) {
  vec3 v;
  if (median) {
    vec3 a[25];
    int k = 0;
    for (int i = 0; i < 5; ++i) {
      float x = float(i - 2);
      for (int j = 0; j < 5; ++j) {
        float y = float(j - 2);
        a[k++] = texture(t, c + d * vec2(x, y)).xyz;
      }
    }
    for (int p = 0; p < 24; ++p) {
      for (int i = 0; i < 24; ++i) {
        int j = i + 1;
        vec3 mi = min(a[i], a[j]);
        vec3 ma = max(a[i], a[j]);
        a[i] = mi;
        a[j] = ma;
      }
    }
    v = a[12];
  } else {
    v = texture(t, c).xyz;
  }
  colour = vec4(v, 1.0);
}
)";

// <http://pippin.gimp.org/a_dither/>
static const char *rgb2yuv_fragment = R"(
#version 410 core
layout(location = 0, index = 0) out vec4 colour;
uniform sampler2D t;
uniform ivec2 size;
uniform bool dithering;
uniform bool deep;
uniform bool chromass;
in vec2 c;
float dither(ivec3 p) {
  int m = (p.x * 125 + p.y * 81 + p.z * 67) & 255;
  return float(m) / 255.0;
}
float nonlinear(float l) {
  l = clamp(l, 0.0, 1.0);
  if (l >= 0.018) {
    return 1.099 * pow(l, 0.45) - 0.099;
  } else {
    return 4.500 * l;
  }
}
vec3 nonlinear(vec3 l) {
  return vec3(nonlinear(l.x), nonlinear(l.y), nonlinear(l.z));
}
void main(void) {
  int k = int(floor(c.x * float(size.x))) + size.x * int(floor(c.y * float(chromass ? size.y + size.y/2 : size.y * 3)));
  int channel = 0;
  if (k >= size.x * size.y) {
    k -= size.x * size.y;
    channel++;
    if (chromass) {
      if (k >= (size.x / 2) * (size.y / 2)) {
        k -= (size.x / 2) * (size.y / 2);
        channel++;
      }
    } else {
      if (k >= (size.x) * (size.y)) {
        k -= (size.x) * (size.y);
        channel++;
      }
    }
  }
  if (channel == 0) {
    vec3 cspace = vec3(0.2126, 0.7152, 0.0722);
    ivec2 ij = ivec2(mod(k, size.x), k / size.x);
    vec3 rgb = texelFetch(t, ij, 0).rgb;
    rgb = nonlinear(rgb);
    float y = dot(cspace, rgb);
    y = 219.0 * y + 16.0;
    if (deep) { y *= 256.0; }
    if (dithering) {
      y += dither(ivec3(ij, channel));
    } else {
      y += 0.5;
    }
    float q;
    if (deep) {
      q = clamp(floor(y + 0.5), 16.0 * 256.0, 236.0 * 256.0 - 1.0) / 65535.0;
    } else {
      q = clamp(floor(y + 0.5), 16.0, 235.0) / 255.0;
    }
    colour = vec4(q);
  } else {
    vec3 cspace = channel == 1
      ? vec3(-0.2126, -0.7152,  0.9278) / 1.8556
      : vec3( 0.7874, -0.7152, -0.0722) / 1.5748;
    ivec2 ij = chromass
      ? ivec2(mod(k, size.x / 2), k / (size.x / 2))
      : ivec2(mod(k, size.x), k / (size.x));
    vec3 rgb;
    if (chromass) {
      rgb
        = texelFetch(t, 2 * ij + ivec2(0, 0), 0).rgb
        + texelFetch(t, 2 * ij + ivec2(0, 1), 0).rgb
        + texelFetch(t, 2 * ij + ivec2(1, 0), 0).rgb
        + texelFetch(t, 2 * ij + ivec2(1, 1), 0).rgb;
      rgb *= 0.25;
    } else {
      rgb = texelFetch(t, ij, 0).rgb;
    }
    rgb = nonlinear(rgb);
    float y = dot(cspace, rgb);
    y = 224.0 * y + 128.0;
    if (deep) { y *= 256.0; }
    if (dithering) {
      y += dither(ivec3(ij, channel));
    } else {
      y += 0.5;
    }
    float q;
    if (deep) {
      q = clamp(floor(y), 16.0 * 256.0, 241.0 * 256.0 - 1.0) / 65535.0;
    } else {
      q = clamp(floor(y), 16.0, 240.0) / 255.0;
    }
    colour = vec4(q);
  }
}
)";

static bool compile_shader(GLuint program, GLenum type, const char *src) {
  GLuint s = glCreateShader(type);
  glShaderSource(s, 1, &src, 0);
  glCompileShader(s);
  GLint ok = 0;
  glGetShaderiv(s, GL_COMPILE_STATUS, &ok);
  if (ok) {
    glAttachShader(program, s);
    glDeleteShader(s);
  } else {
    char buf[1024];
    buf[0] = 0;
    glGetShaderInfoLog(s, 1024, 0, buf);
    fprintf(stderr, "%s\n", buf);
	}
  return ok;
}

static bool link_program(GLuint p) {
  glLinkProgram(p);
  GLint ok = 0;
  glGetProgramiv(p, GL_LINK_STATUS, &ok);
  if (! ok) {
    char buf[1024];
    buf[0] = 0;
    glGetProgramInfoLog(p, 1024, 0, buf);
    fprintf(stderr, "%s\n", buf);
  }
  return ok;
}

static GLuint compile_program_morph() {
  GLuint p = glCreateProgram();
  compile_shader(p, GL_VERTEX_SHADER, morph_vertex);
  compile_shader(p, GL_GEOMETRY_SHADER, morph_geometry);
  compile_shader(p, GL_FRAGMENT_SHADER, morph_fragment);
  link_program(p);
  return p;
}

static GLuint compile_program_median() {
  GLuint p = glCreateProgram();
  compile_shader(p, GL_VERTEX_SHADER, median_vertex);
  compile_shader(p, GL_FRAGMENT_SHADER, median_fragment);
  link_program(p);
  return p;
}

static GLuint compile_program_rgb2yuv() {
  GLuint p = glCreateProgram();
  compile_shader(p, GL_VERTEX_SHADER, median_vertex);
  compile_shader(p, GL_FRAGMENT_SHADER, rgb2yuv_fragment);
  link_program(p);
  return p;
}

static void *read_file(const char *filename, size_t *size) {
  FILE *f = fopen(filename, "rb");
  fseek(f, 0, SEEK_END);
  *size = ftell(f);
  fseek(f, 0, SEEK_SET);
  void *buf = malloc(*size);
  fread(buf, *size, 1, f);
  fclose(f);
  return buf;
}

extern int main(int argc, char **argv) {
  (void) argc;
  (void) argv;

  int FPS = 30;
  int width = 1920;
  int height = 1080;
  int samples = 8;
  double speedup = 1;
  bool excerpt = false;
  double excerpt_start = 0.25; // percentage
  double excerpt_length = 40; // seconds
  bool filter = true;
  bool median = false;
  bool deep = true;
  bool deepio = false;
  bool record = true;
  bool rgb = true;
  bool chromass = false;
  bool dithering = false;
  int pipeline = 4;

  glfwInit();
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(width, height, "Umstülpung", 0, 0);

  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError();

  GLuint mp = compile_program_median();
  glUseProgram(mp);
  GLint mu_d = glGetUniformLocation(mp, "d");
  GLint mu_median = glGetUniformLocation(mp, "median");
  glUniform2f(mu_d, 1.0 / width, 1.0 / height);
  glUniform1i(mu_median, median ? 1 : 0);

  GLuint yp = compile_program_rgb2yuv();
  glUseProgram(yp);
  GLint yu_t = glGetUniformLocation(yp, "t");
  GLint yu_size = glGetUniformLocation(yp, "size");
  GLint yu_dithering = glGetUniformLocation(yp, "dithering");
  GLint yu_deep = glGetUniformLocation(yp, "deep");
  GLint yu_chromass = glGetUniformLocation(yp, "chromass");
  glUniform1i(yu_t, filter ? 1 : 0);
  glUniform2i(yu_size, width, height);
  glUniform1i(yu_dithering, dithering ? 1 : 0);
  glUniform1i(yu_deep, deepio ? 1 : 0);
  glUniform1i(yu_chromass, chromass ? 1 : 0);

  GLuint p = compile_program_morph();
  glUseProgram(p);
  GLint u_morph = glGetUniformLocation(p, "morph");
  GLint u_projectionMatrix = glGetUniformLocation(p, "projectionMatrix");
  GLint u_modelMatrix = glGetUniformLocation(p, "modelMatrix");
  GLint u_normalMatrix = glGetUniformLocation(p, "normalMatrix");
  GLint u_scale = glGetUniformLocation(p, "scale");
  GLint u_eye = glGetUniformLocation(p, "eye");
  GLint u_material = glGetUniformLocation(p, "material");
  GLint u_ambient = glGetUniformLocation(p, "ambient");
  GLint u_diffuse = glGetUniformLocation(p, "diffuse");
  GLint u_specular = glGetUniformLocation(p, "specular");
  GLint u_index = glGetUniformLocation(p, "index");
  GLint u_roughness = glGetUniformLocation(p, "roughness");
  GLint u_alpha = glGetUniformLocation(p, "alpha");

  float aspect = width / (float) height;
  float size = 7.0f / 4.0f * 10.0 / 9.0;
  glm::mat4 projectionMatrix(glm::ortho(-aspect * size, aspect * size, -size, size, 0.1f, 10.0f));

  glUniformMatrix4fv(u_projectionMatrix, 1, GL_FALSE, &projectionMatrix[0][0]);

  glUniform3f(u_eye, 0, 0, 0);

  // <http://gemologyproject.com/wiki/index.php?title=Obsidian>
  glUniform3f(u_material, 0, 0, 0);
  glUniform1f(u_ambient, 0.01);
  glUniform1f(u_diffuse, 0.2);
  glUniform1f(u_specular, 0.8);
  glUniform1f(u_index, 1.5);
  glUniform1f(u_roughness, 0.15 * 0.15);
  glUniform1f(u_alpha, 1);

  GLuint texy;
  glGenTextures(1, &texy);
  glBindTexture(GL_TEXTURE_2D, texy);
  glTexImage2D(GL_TEXTURE_2D, 0, deepio ? GL_R16 : GL_R8, width, chromass ? (height + height/2) : (height * 3), 0, GL_RED, deepio ? GL_UNSIGNED_SHORT : GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fboy;
  glGenFramebuffers(1, &fboy);
  glBindFramebuffer(GL_FRAMEBUFFER, fboy);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texy, 0);
  int bytes = chromass ? width * (height + height / 2) : width * (height * 3);
  if (deepio) { bytes *= 2; }
  GLsync syncs[pipeline];
  GLuint pbo[pipeline];
  glGenBuffers(pipeline, &pbo[0]);
  for (int i = 0; i < pipeline; ++i) {
    glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[i]);
    glBufferStorage(GL_PIXEL_PACK_BUFFER, bytes, 0, GL_MAP_READ_BIT);
  }

  GLuint texd;
  glGenTextures(1, &texd);
  glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, texd);
  glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, GL_DEPTH_COMPONENT, width, height, false);
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, tex);
  glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, samples, deep ? GL_RGBA16 : GL_RGBA8, width, height, false);
  GLuint fbo;
  glGenFramebuffers(1, &fbo);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, tex, 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE, texd, 0);

  GLuint tex2;
  glGenTextures(1, &tex2);
  glBindTexture(GL_TEXTURE_2D, tex2);
  glTexImage2D(GL_TEXTURE_2D, 0, deep ? GL_RGBA16 : GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fbo2;
  glGenFramebuffers(1, &fbo2);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2, 0);

  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);
  GLuint ebo;
  glGenBuffers(1, &ebo);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
  size_t cuboloid_indices_size = 0;
  GLuint *cuboloid_indices = (GLuint *) read_file("cuboloid.u32", &cuboloid_indices_size);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, cuboloid_indices_size, cuboloid_indices, GL_STATIC_DRAW);
  free(cuboloid_indices);

  GLuint vbo;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  size_t cuboloid_vertices_size = 0;
  GLfloat *cuboloid_vertices = (GLfloat *) read_file("cuboloid.f32", &cuboloid_vertices_size);
  glBufferData(GL_ARRAY_BUFFER, cuboloid_vertices_size, cuboloid_vertices, GL_STATIC_DRAW);
  free(cuboloid_vertices);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), ((unsigned char *) 0) + 3 * sizeof(GLfloat));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glActiveTexture(GL_TEXTURE1);
  GLuint vaom;
  glGenVertexArrays(1, &vaom);
  GLuint texm;
  glGenTextures(1, &texm);
  glBindTexture(GL_TEXTURE_2D, texm);
  glTexImage2D(GL_TEXTURE_2D, 0, deep ? GL_RGBA16 : GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  GLuint fbom;
  glGenFramebuffers(1, &fbom);
  glBindFramebuffer(GL_FRAMEBUFFER, fbom);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texm, 0);
  glActiveTexture(GL_TEXTURE0);

  if (deepio && rgb) {
    glPixelStorei(GL_PACK_SWAP_BYTES, GL_TRUE);
  }

  // average earth-sun light minutes 8.317
  double t0 = 8.317 * 60 / speedup;

  // whole numbers of frames for looping
  int frames = round(2 * t0 * FPS);
  t0 = frames / (2.0 * FPS);
  fprintf(stderr, "%d frames = 2 * %f seconds * %d frames per second\n", frames, t0, FPS);

  // excerpt
  int frame0 = 0;
  int frame1 = frames / 2;
  if (excerpt) {
    frame0 = excerpt_start * frames;
    frame1 = excerpt_length * FPS;
  }

  int frame = 0;
  while (! glfwWindowShouldClose(window)) {
    double f = (frame + frame0) % frames;
    double x = f / (double) frames;
    double z = x;
    double w = x;
    float h = fmod(9 * w, 1);
    float s = w < 0.25 ? 4 * w : w > 0.75 ? 4 * (1 - w) : 1;
    s = 0.5 - 0.5 * cos(pi * s);
    float v = w < 0.5 ? 1 - 2 * w : 2 * w - 1;
    float r, g, b;
    hsv2rgb(h, s, v, &r, &g, &b);
    glClearColor(1 - r, 1 - g, 1 - b, 1);

    glEnable(GL_DEPTH_TEST);
    if (filter) {
      glBindFramebuffer(GL_FRAMEBUFFER, fbo);
    } else if (record) {
      glBindFramebuffer(GL_FRAMEBUFFER, fbo2);
    } else {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
      
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindVertexArray(vao);
    glUseProgram(p);
    glUniform1f(u_scale, 1);
    glUniform3f(u_material, r, g, b);
    glUniform1f(u_morph, 0.5 + 0.5 * cos(twopi * x));
    glm::mat4 rot1(glm::rotate(glm::mat4(1.0f), float(twopi * 2 * z), glm::vec3(0.0f, 1.0f, 0.0f)));
    glm::mat4 rot2(glm::rotate(glm::mat4(1.0f), float(twopi * 3 * z), glm::vec3(0.0f, 0.0f, 1.0f)));
    glm::mat4 rot3(glm::rotate(glm::mat4(1.0f), float(twopi * 4 * z), glm::vec3(1.0f, 0.0f, 0.0f)));
    glm::mat4 modelMatrix1(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, -3.0f)));
    glm::mat4 modelMatrix(modelMatrix1 * rot1 * rot2 * rot3);
    glUniformMatrix4fv(u_modelMatrix, 1, GL_FALSE, &modelMatrix[0][0]);
    glm::mat3 normalMatrix(glm::inverse(glm::transpose(glm::mat3(modelMatrix))));
    glUniformMatrix3fv(u_normalMatrix, 1, GL_FALSE, &normalMatrix[0][0]);

    glDrawElements(GL_TRIANGLES, cuboloid_indices_size / sizeof(GLuint), GL_UNSIGNED_INT, 0);

    if (filter) {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
    glDisable(GL_DEPTH_TEST);

    if (filter) {
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo2);
      glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
      glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
      if (record) {
        glBindFramebuffer(GL_FRAMEBUFFER, fbom);
      } else {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
      }
      glBindVertexArray(vaom);
      glUseProgram(mp);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

    if (record) {
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
      glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
      glBindFramebuffer(GL_READ_FRAMEBUFFER, filter ? fbom : fbo2);
      glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST);

      int k = frame % pipeline;
      glBindBuffer(GL_PIXEL_PACK_BUFFER, pbo[k]);
      GLsync s = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
      if (frame - pipeline >= 0) {
        glWaitSync(syncs[k], 0, GL_TIMEOUT_IGNORED);
        void *buf = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, bytes, GL_MAP_READ_BIT);
        if (rgb) {
          fprintf(stdout, "P6\n%d %d\n%d\n", width, height, deepio ? 65535 : 255);
        }
        fwrite(buf, bytes, 1, stdout);
        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
      }
      syncs[k] = s;
      if (rgb) {
        glReadPixels(0, 0, width, height, GL_RGB, deepio ? GL_UNSIGNED_SHORT : GL_UNSIGNED_BYTE, 0);
      } else {
        glViewport(0, 0, width, chromass ? (height + height / 2) : (height * 3));
        glBindFramebuffer(GL_FRAMEBUFFER, fboy);
        glBindVertexArray(vaom);
        glUseProgram(yp);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glReadPixels(0, 0, width, chromass ? (height + height / 2) : (height * 3), GL_RED, deepio ? GL_UNSIGNED_SHORT : GL_UNSIGNED_BYTE, 0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glViewport(0, 0, width, height);
      }
    }

    { int e = glGetError(); if (e) { fprintf(stderr, "%d: %d\n", __LINE__, e); } }
    glfwSwapBuffers(window);

    frame += 1;
    if (record && frame == frame1 + pipeline) {
      break;
    }
    glfwPollEvents();
  }

  return 0;
}
