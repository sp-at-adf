import Control.Monad (forM_)
import Data.Ix (inRange)
import Data.List (intercalate)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Word (Word32)
import Foreign (with)
import System.IO (withBinaryFile, IOMode(WriteMode), hPutBuf)

data Vertex = V{ v't, v'v, v'ox, v'oy, v'oz, v'cx, v'cy, v'cz, v'nx, v'ny, v'nz :: !Double, v'e :: !Bool }
  deriving (Read, Show, Eq, Ord)

vertex :: Double -> Double -> Vertex
vertex t v = V t v ox oy oz cx cy cz nx ny nz False
  where
    -- oloid param
    u = 2 * pi / 3 * signum t * (3 * abs t - 2 * abs t ** 1.5)
    c = (-1/2) `max` cos u
    s = signum t * sqrt (0 `max` (1 - c * c))
    -- oloid arcs
    ox0 = s
    oy0 = - c - 1/2
    oz0 = 0
    ox1 = 0
    oy1 = 1 / (1 + c) - 1/2
    oz1 = signum v * sqrt (1 + 2 * c) / (1 + c)
    -- oloid surface
    ox = ox0 * (1 - abs v) + abs v * ox1
    oy = oy0 * (1 - abs v) + abs v * oy1
    oz = oz0 * (1 - abs v) + abs v * oz1
    -- sphere surface
    sl = sqrt (ox * ox + oy * oy + oz * oz)
    sx = ox / sl
    sy = oy / sl
    sz = oz / sl
    -- cube surface
    cl = abs sx `max` abs sy `max` abs sz
    cx = sx / cl
    cy = sy / cl
    cz = sz / cl
    -- cube normal
    nl = abs cx `max` abs cy `max` abs cz
    nx = if abs cx == nl then signum cx else 0
    ny = if abs cy == nl then signum cy else 0
    nz = if abs cz == nl then signum cz else 0

type VID = (Int, Int)

data Mesh = M{ m'n :: !Int, m'v :: !(Map VID Vertex) }
  deriving (Read, Show, Eq, Ord)

mesh :: Int -> Mesh
mesh n = M n (Map.fromList [ ((i, j), vertex (f i) (f j)) | i <- [0 .. n], j <- [0 .. n] ])
  where
    f t = (fromIntegral t / fromIntegral n) * 2 - 1

edges :: Int -> [(VID, VID)]
edges n =
  [ ((i, j), v)
  | i <- [0 .. n]
  , j <- [0 .. n]
  , v <- [ (i, j + 1), (i + 1, j), (i + 1, j + 1) ]
  , inRange ((0, 0), (n, n)) v
  ]

crossesCubeEdge :: Vertex -> Vertex -> Bool
crossesCubeEdge V{ v'nx = ax, v'ny = ay, v'nz = az } V{ v'nx = bx, v'ny = by, v'nz = bz } =
  ax * bx + ay * by + az * bz == 0

splitEdge :: Vertex -> Vertex -> Vertex
splitEdge x y
  | v'e x || v'e y = mid x y
  | crossesCubeEdge x y = (search 64 x (mid x y) y){ v'e = True }
  | otherwise = mid x y
  where
    mid a b = vertex ((v't a + v't b) / 2) ((v'v a + v'v b) / 2)
    search 0 _ b _ = b
    search n a b c = case (crossesCubeEdge a b, crossesCubeEdge b c) of
      (False, False) -> b
      (True,  False) -> search (n - 1) a (mid a b) b
      (False, True ) -> search (n - 1) b (mid b c) c
      (True,  True ) -> search (n - 1) a (mid a b) b -- crosses 2 edges, near corner?

refine :: Mesh -> Mesh
refine (M n0 vs0) = M n vs
  where
    n = 2 * n0
    vs1 = Map.mapKeysMonotonic (\(i,j) -> (2 * i, 2 * j)) vs0
    vs2 = Map.fromList
            [ ((i0 + i1, j0 + j1), splitEdge (vs0 Map.! ij0) (vs0 Map.! ij1))
            | (ij0@(i0, j0), ij1@(i1, j1)) <- edges n0
            ]
    vs = Map.union vs1 vs2

canonical :: Int -> VID -> VID
canonical n (i, j)
  | i == 0 = (i, min j (n - j))
  | j == 0 = (min i (n - i), j)
  | i == n = (i, min j (n - j))
  | j == n = (min i (n - i), j)
  | otherwise = (i, j)

index :: Int -> VID -> Int
index n (i, j) = i + (n + 1) * j

triangles :: Int -> [Int]
triangles n = map (index n . canonical n) . concat $
  [ [ (i, j), (i + 1, j), (i + 1, j + 1)
    , (i, j), (i + 1, j + 1), (i, j + 1) ]
  | i <- [0 .. n - 1]
  , j <- [0 .. n - 1]
  ]

vertices :: Map VID Vertex -> [Double]
vertices vs = concat
  [ [ ox, oy, oz, cx, cy, cz ] | V _ _ ox oy oz cx cy cz _ _ _ _ <- Map.elems vs ]

pretty :: Mesh -> String
pretty (M n vs) =
  "GLuint cuboloid_indices[] = {\n  " ++
  intercalate "," (map show (triangles n)) ++
  "\n};\nGLfloat cuboloid_vertices[] = {\n  " ++
  intercalate "," (map show (vertices vs)) ++
  "\n};\n"

main :: IO ()
main = do
  let n = 1024
  withBinaryFile "cuboloid.u32" WriteMode $ \h -> do
    forM_ (map (fromIntegral :: Int -> Word32) . triangles . (2 *) $ n) $ \w -> do
      with w $ \p -> hPutBuf h p 4
  withBinaryFile "cuboloid.f32" WriteMode $ \h -> do
    forM_ (map (realToFrac :: Double -> Float) . vertices . m'v . refine . mesh $ n) $ \w -> do
      with w $ \p -> hPutBuf h p 4

{-
-- for gnuplotting
main
  = writeFile "mesh-rgb.dat"
  . unlines
  . map (\v -> unwords [show $ v't v, show $ v'v v, show $ rgb (v'nx v) (v'ny v) (v'nz v)])
  . filter v'e
  . Map.elems
  . m'v
  . refine
  . mesh
  $ 128
  where
    rgb r g b = 65536 * c r + 256 * c g + c b
    c x = round $ 0 `max` 255 * (x + 1) / 2 `min` 255
-}
